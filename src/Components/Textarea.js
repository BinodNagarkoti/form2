import React from "react";

export default function Input(props) {
  return (
    <div>
      <span> {props.label}: </span>
      <div>
        <textarea
          style={{ marginTop: "-15px", marginLeft: "62px" }}
          {...props}
        />
      </div>
    </div>
  );
}
