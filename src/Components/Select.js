import React from "react";
export default function Select(props) {
  const { options, id, label, value, onChange } = props;
  // const[selected,setSelected]=useState("")
  // console.log(props);
  return (
    <div>
      <label> {label} </label>
    <select id={id} value={value} onChange={onChange}>
      {options.map((item, index) => (
        <option key={index}>{item.value}</option>
      ))}
    </select>
    </div>
  );
}
