import React, { Component } from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";

// import Input from "./Components/Input";
// import Textarea from "./Components/Textarea";
// import Select from "./Components/Select";
import {
  Button,
  Table,
  Alert,
  ListGroup,
  Container,
  Row,
  Col,
  Form
} from "react-bootstrap/";

class App extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      email: "",
      selectedDesignation: "",
      selectedTiming: "",
      location: "",
      defaultDesignation: [
        { value: "Select Designation", label: "null" },
        { value: "Intern", label: "Intern" },
        { value: "Junior", label: "Junior" },
        { value: "Senior", label: "Senior" },
        { value: "Leader", label: "Leader" },
        { value: "Mid", label: "Mid" }
      ],
      defaultTiming: [
        { value: "Select Timing" },
        { value: "8-4" },
        { value: "7-4" },
        { value: "10-6" },
        { value: "9-5" }
      ],
      employee: [],
      message: ""
    };
    this.handleChange = this.handleChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  onSubmit = async e => {
    e.preventDefault();
    if (!this.isEmpty()) {
      const {
        selectedDesignation,
        selectedTiming,
        name,
        email,
        location
      } = this.state;
      const newEmployee = {
        name: name,
        email: email,
        location: location,
        timing: selectedTiming,
        designation: selectedDesignation
      };
      console.log("newEmployee", newEmployee);
      if (this.isUnique(email)) {
        await this.setState(preState => {
          return {
            name: "",
            email: "",
            location: "",
            timing: "",
            designation: "",
            message: "",
            employee: preState.employee.concat(newEmployee)
          };
        });
        console.log("newSateEmployee", this.state.employee);
      } else {
        this.setState({ message: "Email must be Unique" });
        // console.log(this.state.message);
      }
    }
  };
  isEmpty() {
    let message = "";
    if (this.state.name === "") {
      message = "Name cannot be Empty";
    } else if (this.state.email === "") {
      message = "email cannot be empty";
    } else if (this.state.location === "") {
      message = "Location cannot be Empty";
    } else if (this.state.selectedDesignation === "") {
      message = "selectedDesignation cannot be empty";
    } else if (this.state.selectedTiming === "") {
      message = "selectedTiming cannot be empty";
    } else {
      message = "";
    }
    this.setState({ message });
    if (message === "") return false;
    return true;
  }
  isUnique(newEmail) {
    const found = this.state.employee.find(item => item.email === newEmail);
    return typeof found === "undefined" ? true : false;
  }
  handleChange = e => {
    const { id, value } = e.target;
    this.setState({
      [id]: value
    });
  };

  render() {
    const defaultDesignation = this.state.defaultDesignation;
    const defaultTiming = this.state.defaultTiming;
    const employees = this.state.employee;
    const message = this.state.message;

    return (
      <Container>
        <header className="App-header">Forms App 2.0</header>
        <br />
        <Form>
          <Row>
            <Col>
              <Form.Group>
                {/* <Form.Label>Name</Form.Label> */}

                <Form.Control
                  type="name"
                  value={this.state.name}
                  onChange={this.handleChange}
                  name="name"
                  id="name"
                  placeholder="Enter Your Name"
                />
              </Form.Group>
            </Col>
            <Col>
              <Form.Group>
                {/* <Form.Label>Select designation</Form.Label> */}
                <Form.Control
                  as="select"
                  id="selectedDesignation"
                  onChange={this.handleChange}
                >
                  {defaultDesignation.map((item, index) => (
                    <option key={index}>{item.value}</option>
                  ))}
                  >
                </Form.Control>
              </Form.Group>
            </Col>
            <Col>
              <Form.Group>
                {/* <Form.Label>Email</Form.Label> */}
                <Form.Control
                  type="email"
                  value={this.state.email}
                  onChange={this.handleChange}
                  name="email"
                  id="email"
                  placeholder=" Enter Your Email"
                />
                <Form.Text className="text-muted">
                  We'll never share your email with anyone else.
                </Form.Text>
              </Form.Group>
            </Col>
            <Col>
              <Form.Group>
                {/* <Form.Label> Location </Form.Label> */}

                <Form.Control
                  type="textarea"
                  rows="3"
                  value={this.state.location}
                  onChange={this.handleChange}
                  name="location"
                  id="location"
                  label="location"
                  placeholder="Enter Your Location"
                />
              </Form.Group>
            </Col>
            <Col>
              <Form.Group>
                {/* <Form.Label>Select Timing</Form.Label> */}
                <Form.Control
                  as="select"
                  id="selectedTiming"
                  onChange={this.handleChange}
                >
                  {defaultTiming.map((item, index) => (
                    <option key={index}>{item.value}</option>
                  ))}
                  >
                </Form.Control>
              </Form.Group>
            </Col>
            <Col>
              <Button
                variant="primary"
                style={{ marginTop: "0px" }}
                type="submit"
                onClick={this.onSubmit}
              >
                Submit
              </Button>
            </Col>
          </Row>
        </Form>
        <Row>
          {" "}
          {message === "" ? (
            ""
          ) : (
            <Alert variant="danger">Oh snap! you got an Error: {message}</Alert>
          )}
        </Row>

        {this.state.employee.length === 0 ? (
          ""
        ) : (
          <div>
            <Row>
              <label>Lists Of Employes: </label>
            </Row>
            <Table>
              <thead>
                <tr>
                  <th>#</th>
                  <th>First Name</th>
                  <th>Email</th>
                  <th>Location</th>
                  <th>Designation</th>
                  <th>Timing</th>
                </tr>
              </thead>
              <tbody>
                {employees.map((employee, index) => (
                  <tr horizontal key={index}>
                    <td>{index + 1} </td>
                    <td> {employee.name}</td>
                    <td> {employee.email}</td>
                    <td> {employee.location}</td>
                    <td> {employee.designation}</td>
                    <td> {employee.timing}</td>{" "}
                  </tr>
                ))}
              </tbody>
            </Table>
          </div>
        )}
      </Container>
    );
  }
}

export default App;
